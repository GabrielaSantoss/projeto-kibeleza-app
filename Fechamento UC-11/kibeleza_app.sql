-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 09-Jun-2020 às 03:36
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `kibeleza_app`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL,
  `nomeCli` varchar(100) NOT NULL,
  `emailCli` varchar(100) NOT NULL,
  `senhaCli` varchar(45) NOT NULL,
  `statusCli` varchar(45) NOT NULL,
  `dataCadCli` date NOT NULL,
  `fotoCli` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`idcliente`, `nomeCli`, `emailCli`, `senhaCli`, `statusCli`, `dataCadCli`, `fotoCli`) VALUES
(1, 'Gabriela', 'gabrielassilva33@gmail.com', '123', 'Ativo', '2020-05-14', 'user/usuario.png'),
(2, 'Carol', 'carol@gmail.com', '123', 'Ativo', '2020-05-18', 'user/usuario.png'),
(3, 'Gabi', 'gabi@123.com', '123', 'Ativo', '2020-05-23', 'user/usuario.png'),
(4, 'Isabella', 'isabella@gmail.com', '123', 'ativo', '2020-05-23', 'user/usuario.png'),
(49, 'Isabel', 'isabel@gmail.com', '123', 'ativo', '2020-05-23', 'user/usuario.jpg'),
(50, 'ei', 'eeiii', '123', 'ativo', '2020-05-23', 'user/usuario.jpg'),
(51, '', '', '', 'ativo', '2020-06-04', 'user/usuario.jpg'),
(52, 'Geise', 'geise@gmail.com', '1234', 'ativo', '2020-06-06', 'user/usuario.jpg'),
(53, 'Lilian', 'lili@gmail.com', '1234', 'ativo', '2020-06-06', 'user/usuario.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `fone` varchar(20) NOT NULL,
  `mens` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa`
--

CREATE TABLE `empresa` (
  `idempresa` int(11) NOT NULL,
  `nomeEmp` varchar(100) NOT NULL,
  `cnpj` varchar(45) NOT NULL,
  `razaoSocial` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `statusEmp` varchar(45) NOT NULL,
  `dataCadEmp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `horarioAtend` time NOT NULL DEFAULT '00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE `funcionario` (
  `idfuncionario` int(11) NOT NULL,
  `nomeFunc` varchar(100) NOT NULL,
  `emailFunc` varchar(100) NOT NULL,
  `senhaFunc` varchar(45) NOT NULL,
  `nivelFunc` varchar(45) NOT NULL,
  `statusFunc` varchar(45) NOT NULL,
  `dataCadFunc` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `horarioTrabalho` time NOT NULL DEFAULT '00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `funcionario`
--

INSERT INTO `funcionario` (`idfuncionario`, `nomeFunc`, `emailFunc`, `senhaFunc`, `nivelFunc`, `statusFunc`, `dataCadFunc`, `horarioTrabalho`) VALUES
(3, 'Gabi', 'Gabi123@gmail.com', '123', 'admin', 'ativo', '2020-06-04 13:50:48', '10:46:52'),
(4, 'Day', 'Day123@gmail.com', '123', 'admin', 'ativo', '2020-06-04 13:50:57', '10:46:52'),
(5, 'Carol', 'Ca123@gmail.com', '123', 'admin', 'ativo', '2020-06-04 13:51:02', '10:46:52');

-- --------------------------------------------------------

--
-- Estrutura da tabela `reserva`
--

CREATE TABLE `reserva` (
  `idreserva` int(11) NOT NULL,
  `obs` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `dataReserva` datetime NOT NULL DEFAULT current_timestamp(),
  `horaReserva` time NOT NULL,
  `status` varchar(250) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `idFunc` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `idServico` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `reserva`
--

INSERT INTO `reserva` (`idreserva`, `obs`, `dataReserva`, `horaReserva`, `status`, `idFunc`, `idCliente`, `idServico`) VALUES
(24, '', '2020-06-06 17:23:59', '17:23:59', '', 4, 1, 2),
(25, '', '2020-06-06 17:23:59', '17:23:59', '', 4, 1, 2),
(31, '', '2020-06-06 19:09:24', '19:09:24', '', 3, 49, 4),
(33, '', '2020-06-06 19:10:45', '19:10:45', '', 5, 2, 1),
(50, '', '0000-00-00 00:00:00', '00:00:03', '3', 3, 3, 3),
(71, '', '2020-08-08 19:00:28', '19:27:56', 'pendente', 3, 1, 1),
(72, '', '2020-06-14 15:00:09', '20:09:42', 'pendente', 3, 1, 1),
(73, '', '2020-06-10 20:00:08', '20:33:25', 'pendente', 5, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `servico`
--

CREATE TABLE `servico` (
  `idservico` int(11) NOT NULL,
  `nomeServico` varchar(100) NOT NULL,
  `valorServico` decimal(10,0) NOT NULL,
  `statusServico` varchar(45) NOT NULL,
  `dataCadServico` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `fotoServico` varchar(250) NOT NULL,
  `fotoServico1` varchar(250) NOT NULL,
  `fotoServico2` varchar(250) NOT NULL,
  `fotoServico3` varchar(250) NOT NULL,
  `descServico` text NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `servico`
--

INSERT INTO `servico` (`idservico`, `nomeServico`, `valorServico`, `statusServico`, `dataCadServico`, `fotoServico`, `fotoServico1`, `fotoServico2`, `fotoServico3`, `descServico`, `texto`) VALUES
(1, 'serviço 01', '60', 'Ativo', '2020-05-05 16:56:10', 'servico/foto.png', 'servico/foto01.png', 'servico/foto02.png', 'servico/foto03.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged'),
(2, 'serviço 02', '60', 'Ativo', '2020-05-05 16:57:15', 'servico/foto.png', 'servico/foto01.png', 'servico/foto02.png', 'servico/foto03.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged'),
(3, 'serviço 03', '60', 'Ativo', '2020-05-11 15:05:21', 'servico/foto.png', 'servico/foto01.png', 'servico/foto02.png', 'servico/foto03.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged'),
(4, 'serviço 04', '60', 'Ativo', '2020-05-11 15:02:14', 'servico/foto.png', 'servico/foto01.png', 'servico/foto02.png', 'servico/foto03.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`) USING BTREE;

--
-- Índices para tabela `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Índices para tabela `funcionario`
--
ALTER TABLE `funcionario`
  ADD PRIMARY KEY (`idfuncionario`);

--
-- Índices para tabela `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`idreserva`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `idFunc` (`idFunc`),
  ADD KEY `idServico` (`idServico`);

--
-- Índices para tabela `servico`
--
ALTER TABLE `servico`
  ADD PRIMARY KEY (`idservico`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de tabela `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `empresa`
--
ALTER TABLE `empresa`
  MODIFY `idempresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `funcionario`
--
ALTER TABLE `funcionario`
  MODIFY `idfuncionario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `reserva`
--
ALTER TABLE `reserva`
  MODIFY `idreserva` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT de tabela `servico`
--
ALTER TABLE `servico`
  MODIFY `idservico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `reserva`
--
ALTER TABLE `reserva`
  ADD CONSTRAINT `reserva_ibfk_1` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idcliente`),
  ADD CONSTRAINT `reserva_ibfk_2` FOREIGN KEY (`idFunc`) REFERENCES `funcionario` (`idfuncionario`),
  ADD CONSTRAINT `reserva_ibfk_3` FOREIGN KEY (`idServico`) REFERENCES `servico` (`idservico`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
