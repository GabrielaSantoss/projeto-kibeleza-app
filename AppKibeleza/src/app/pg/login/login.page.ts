import { Component, OnInit } from '@angular/core';
import {  NavController } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { Http } from '@angular/http';
import { UrlService} from '../../servidor/url.service';
import { UsuarioService} from '../../servidor/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email:any;
  senha:any;

  constructor(
    public http: Http, 
    public servidorUrl:UrlService, 
    public dadosUsuario: UsuarioService, 
    public nav: NavController) { 

      this.email='gabrielassilva33@gmail.com';
      this.senha='123';

      if(localStorage.getItem('deslogado') == 'sim'){
        localStorage.setItem('deslogado','não');
        location.reload();
      }
      if(localStorage.getItem('userLogado') != null){
        this.autenticarUser();
        console.log('User foi autenticado');
        this.nav.navigateBack('/tabs/pg/home');
      }


     }

  async Entrar() {
    if(this.email == undefined || this.senha == undefined){

     this.servidorUrl.alertas("Atenção", "Preencha todos os campos.");

    }else{
      
      this.http.get(this.servidorUrl.pegarUrl()+'login.php?email='+this.email+'&senha='+this.senha)
      .pipe(map(res => res.json()))
      .subscribe(data => {

        if(data.msg.logado == 'sim'){
          if(data.dados.statusCli == 'ativo' ){
            this.servidorUrl.alertas("Ok", "Logado com sucesso.");

            // Alimentar as variáveis 
            this.dadosUsuario.setIdUsuario(data.dados.idcliente);
            this.dadosUsuario.setNomeUsuario(data.dados.nomeCli);
            this.dadosUsuario.setEmailUsuario(data.dados.emailCli);
            this.dadosUsuario.setStatusUsuario(data.dados.statusCli);
            this.dadosUsuario.setFotoUsuario(data.dados.fotoCli);

            //Armazenar os dados no Local Storage
            localStorage.setItem('idcliente', data.dados.idcliente);
            localStorage.setItem('nomeCliente', data.dados.nomeCli);
            localStorage.setItem('emailCliente', data.dados.emailCli);
            localStorage.setItem('statusCliente', data.dados.statusCli);
            localStorage.setItem('fotoCliente', data.dados.fotoCli);

            localStorage.setItem('userLogado', data);

            this.nav.navigateBack('/tabs/pg/perfil');

          }else{
            
            this.servidorUrl.alertas("Atenção", "Entre em contato com o Admin.");
          }
        }else{
          this.servidorUrl.alertas("Atenção", "Usuário ou Senha invalido");
        }
      });
    }
  }

  autenticarUser(){
    this.dadosUsuario.setIdUsuario(localStorage.getItem('idCliente'));
    this.dadosUsuario.setNomeUsuario(localStorage.getItem('nome'));
    this.dadosUsuario.setEmailUsuario(localStorage.getItem('email'));
    this.dadosUsuario.setStatusUsuario(localStorage.getItem('status'));
    this.dadosUsuario.setFotoUsuario(localStorage.getItem('foto'));
  }

  ngOnInit() {
    document.querySelector('ion-tab-bar').style.display = 'none';
  }

}