import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  idusuario = '';
  nomeUsuario = '';
  emailUsuario = '';
  statusUsuario = '';
  senhaUsuario = '';
  fotoUsuario = '';


  constructor() { }

  //GET (Assessores) e SET (modificadores)
  //ID
  setIdUsuario(valor){
    this.idusuario = valor;
  }
  getIdUsuario(){
    return this.idusuario;
  }
  //NOME
  setNomeUsuario(valor){
    this.nomeUsuario = valor;
  }
  getNomeUsuario(){
    return this.nomeUsuario;
  }
  //EMAIL
  setEmailUsuario (valor){
    this.emailUsuario = valor;
  }
  getEmailUsuario(){
    return this.emailUsuario;
  }
  //STATUS
  setStatusUsuario(valor){
    this.statusUsuario = valor;
  }
  getStatusUsuario(){
    return this.statusUsuario;
  }
   //SENHA
   setSenhaUsuario(valor){
    this.senhaUsuario = valor;
  }
  getSenhaUsuario(){
    return this.senhaUsuario;
  }
   //FOTO
   setFotoUsuario(valor){
    this.fotoUsuario = valor;
  }
  getFotoUsuario(){
    return this.fotoUsuario;
  }
}
