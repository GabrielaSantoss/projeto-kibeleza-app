import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validator, Validators, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Http, Headers, Response } from '@angular/http';
import { map } from 'rxjs/operators';
import { UrlService } from '../../servidor/url.service';
import { NavController } from '@ionic/angular';
import * as moment from 'moment';

@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.page.html',
  styleUrls: ['./reserva.page.scss'],
})
export class ReservaPage implements OnInit {

  id:any;
  funcs: any;
  
  dataMin: String = moment().format();
  
  reserva:any;
  codFunc:any;
  dataReserva:any;
  idCliente = localStorage.getItem('idcliente');
  idServico = localStorage.getItem('codServico');
  
  constructor(public formConst:FormBuilder,
              public http: Http,
              public servidorUrl: UrlService,
              public nav: NavController) { 

              
                console.log(this.codFunc );
                console.log(this.idCliente );


          //Para Verificar o Login
          this.verificarLogin();

      
          //Montar Form
          this.reserva = this.formConst.group({
            codFunc: ['', Validators.required],
            dataReserva: ['', Validators.required],
            idCliente: this.idCliente,
            idServico:this.idServico,
          });

        this.listaFunc();    
  } // ##Fim Constructor####

  ngOnInit() {
  }

  
  //######## cad dados ###
  cadDados(nomeFunc){
    let cabecalho = new Headers({'content-type':'application/x-www-form-urlencoded'});

    return this.http.post(this.servidorUrl.pegarUrl()+'reserva.php', nomeFunc, {
      headers: cabecalho,
      method: 'POST'
    })
    .pipe(map((res: Response) => {
      return res.json();
    }));
  }
 // Fim Cad Dadosm// reserva

//######## começo reserva ########
  async cadReserva(){
    console.log(localStorage.getItem('nomeCliente'));
    if(this.codFunc == undefined || this.dataReserva == undefined){
      
      this.servidorUrl.alertas('Atenção', localStorage.getItem('nomeCliente') + ', preencha todos os campos.');
    
    }else{

      this.cadDados(this.reserva.value).subscribe(
        data => {
          console.log("Dados ok");
          this.servidorUrl.alertas('KiBileza', localStorage.getItem('nomeCliente') + ', agendamento  realizado com sucesso');
          this.nav.navigateBack('/tabs/pg/perfil');
        },
        err =>{
          console.log("Dados com Erro");
          this.servidorUrl.alertas('KiBileza', localStorage.getItem('nomeCliente') + ', erro ao realizar o agendamento');
        }
      )
    }
  }
//######## fim reserva ########


 


//Verificar Login
  verificarLogin(){
    if(localStorage.getItem('userLogado') != null){
      console.log("User logado");
    }else{
      this.nav.navigateBack('/tabs/pg/login');
    }
  }

  // ##### Lista funcionarios 

  
  listaFunc(){
    this.http.get(this.servidorUrl.pegarUrl()+"lista-func.php").pipe(map(res => res.json()))
    .subscribe(
      listaDados => {
        this.funcs = listaDados;
        console.log(this.funcs[0].codigo);
        console.log(this.funcs[0].NomeFunc);
      }
    );

}
// ##### Fim lista FUNCS ########################

}
