import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { UrlService } from '../../servidor/url.service';


@Component({
  selector: 'app-detalhe',
  templateUrl: './detalhe.page.html',
  styleUrls: ['./detalhe.page.scss'],
})
export class DetalhePage implements OnInit {

  id: any;
  detalhe:any;
  dados: Array<{ 
    Codigo:any,
    NomeServico:any,
    valorServico:any,
    statusServico:any,
    dataCadServico:any,
    fotoServico:any,
    fotoServico1:any,
    fotoServico2:any,
    fotoServico3:any,
    descServico:any,
    texto:any,  }>

  constructor(public http:Http, public servidorUrl: UrlService, public rota:ActivatedRoute) { 

    this.rota.params.subscribe(parametroId => {
      this.id = parametroId.id;
      this. detalheServico();
      this.dados = [];
      console.log(this.id);
  });
}

  detalheServico(){
    this.http.get(this.servidorUrl.pegarUrl() + 'detalhe-servico.php?idservico=' + this.id)
    .pipe(map(res => res.json()))
    .subscribe(
      data =>{
        this.detalhe = data;
        console.log(this.detalhe);
    
        for(let i = 0; i < data.length; i++ ){

          this.dados.push({
            Codigo: data[i]['Codigo'],
            NomeServico: data[i]['NomeServico'],
            valorServico: data[i]['valorServico'],
            statusServico: data[i]['statusServico'],
            dataCadServico: data[i]['dataCadServico'],
            fotoServico: data[i]['fotoServico'],
            fotoServico1: data[i]['fotoServico1'],
            fotoServico2: data[i]['fotoServico2'],
            fotoServico3: data[i]['fotoServico3'],
            descServico: data[i]['descServico'],
            texto: data[i]['texto'],
            

          });


        }
        
        console.log(this.dados[0].texto);
        localStorage.setItem('codServico', this.dados[0].Codigo);

        console.log(this.dados[0].NomeServico);

      }
    );
  }

  ngOnInit() {
  }
}
  
 

