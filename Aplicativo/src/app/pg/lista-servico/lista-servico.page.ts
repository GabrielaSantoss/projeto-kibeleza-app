import { Component, OnInit } from '@angular/core';
import {map} from 'rxjs/operators';
import {Http} from '@angular/http';
import {UrlService} from '../../servidor/url.service';

@Component({
  selector: 'app-lista-servico',
  templateUrl: './lista-servico.page.html',
  styleUrls: ['./lista-servico.page.scss'],
})
export class ListaServicoPage implements OnInit {

  servicos:any;
  
  constructor(public http:Http, public servidorUrl:UrlService) { 

    this.listaServico();

  }
//"http://localhost/web-site-kibeleza/admin/";

  listaServico(){
    this.http.get(this.servidorUrl.pegarUrl()+"lista-servico.php").pipe(map(res => res.json()))
    .subscribe(
      listaDados => {
        this.servicos = listaDados;
        console.log(this.servicos[0].fotoServico);
      }
    );

    
  }

  ngOnInit() {
  }

}
