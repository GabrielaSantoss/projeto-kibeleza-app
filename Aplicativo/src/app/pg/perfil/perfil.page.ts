import { Component, OnInit } from '@angular/core';
import { UsuarioService} from '../../servidor/usuario.service';
import { UrlService} from '../../servidor/url.service';
import {  NavController } from '@ionic/angular';
@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  constructor(public dadosUsuario: UsuarioService,
               public nav: NavController,         
               public urlServidor: UrlService) { 

   this.verificarLogin();
  }


  ngOnInit() {
    document.querySelector('ion-tab-bar').style.display = 'flex';
  }

  verificarLogin(){
    if(localStorage.getItem('userLogado') != null){
      console.log('Usuário Logado');

      //Pegar Infonmações do LocalStorange
      
        this.dadosUsuario.setIdUsuario(localStorage.getItem('idCliente'));
        this.dadosUsuario.setNomeUsuario(localStorage.getItem('nomeCliente'));
        this.dadosUsuario.setEmailUsuario(localStorage.getItem('emailCliente'));
        this.dadosUsuario.setStatusUsuario(localStorage.getItem('statusCliente'));
        this.dadosUsuario.setFotoUsuario(localStorage.getItem('fotoCliente'));

    

    }else{
      this.nav.navigateBack('/tabs/pg/login');
    }
  }
  sairLogin(){
    localStorage.clear();
    location.reload();
    localStorage.setItem('deslogado', 'sim');
    this.nav.navigateBack('/tabs/pg/home');
  }

}

