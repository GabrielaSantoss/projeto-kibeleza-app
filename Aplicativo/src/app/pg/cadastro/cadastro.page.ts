import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validator, Validators } from '@angular/forms';
import { Http, Headers, Response } from '@angular/http';
import { map, windowWhen } from 'rxjs/operators';
import { UrlService} from '../../servidor/url.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  cadastro: any;
  nome: any;
  email: any;
  senha: any;

  constructor(public formConst:FormBuilder,
              public http: Http,
              public nav: NavController,
              public servidorUrl: UrlService) {
 
                this.cadastro = this.formConst.group({
                  nome: ['', Validators.required],
                  email: ['', Validators.required],
                  senha: ['', Validators.required],

                });

              
  }
     async cadCliente(){
        if(this.nome == undefined || this.email == undefined || this.senha == undefined){
          this.servidorUrl.alertas('Atenção','Preencha todos os campos. ');
        
        }else{
         this.cadDados(this.cadastro.value).subscribe( 
          data => {
             console.log("Dados ok");
             this.servidorUrl.alertas('Kibeleza', this.nome + ', seu cadastro foi realizado com sucesso');
              this.nav.navigateBack('/tabs/pg/login');
            },
           err => {
            console.log("Dados Erro");
            this.servidorUrl.alertas('Kibeleza', this.nome + ', erro ao cadastrar');
           }
          )
        }
 
     }


     cadDados (nome){
       let cabecalho = new Headers({'content-type':'application/x-www-form-urlencoded'});
       
       return this.http.post(this.servidorUrl.pegarUrl()+'cadastro.php', nome,{
       headers: cabecalho,
       method: 'POST'
    })
    .pipe(map((res: Response) => {
      return res.json();
    }));

     }// Fim Cad Dados


  ngOnInit() {
    document.querySelector('ion-tab-bar').style.display = 'none';
  }

}
