import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UrlService {

  url:string= "http://localhost/web-site-kibeleza/admin/";
  //url:string= "http://imperiumdgl.com.br/Gabriela/admin/";
 
  constructor(public alerta:AlertController) { }

  pegarUrl(){
    return this.url
  }

  async alertas(titulo,msg){   const alertLogin = await this.alerta.create({
    header: titulo,
    message: msg,
    buttons: ['OK']
  });

  await alertLogin.present();
  }

}
